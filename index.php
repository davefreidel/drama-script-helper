<?php
require_once(__DIR__ . '/system/inc/common.inc.php');
require_once(__DIR__ . '/system/inc/head.inc.php'); ?>


<div id="menu">
	<div class="site-width">
		<select id="day-picker">
			<option>Pick a Day</option>
			<option <?php if (session_page() == 1){?> selected="selected" <?php } ?> value="1">Day 1</option>
			<option <?php if (session_page() == 2){?> selected="selected" <?php } ?> value="2">Day 2</option>
			<option <?php if (session_page() == 3){?> selected="selected" <?php } ?> value="3">Day 3</option>
			<option <?php if (session_page() == 4){?> selected="selected" <?php } ?> value="4">Day 4</option>
			<option <?php if (session_page() == 5){?> selected="selected" <?php } ?> value="5">Day 5</option>
		</select>
		<select id="role-picker">
			<option value="">Pick a Role</option>
			<option <?php if (session_role() == 'alex'){?> selected="selected" <?php } ?> value="alex">Alex</option>
			<option <?php if (session_role() == 'bruce'){?> selected="selected" <?php } ?> value="bruce">Bruce/Dad/Scuba Man</option>
			<option <?php if (session_role() == 'wayne'){?> selected="selected" <?php } ?> value="wayne">Wayne/Snorkel Boy</option>
			<option <?php if (session_role() == 'turtle'){?> selected="selected" <?php } ?> value="turtle">Mom/Turtle</option>
			<option <?php if (session_role() == 'white'){?> selected="selected" <?php } ?> value="white">Mr. White/Great White</option>
			<option <?php if (session_role() == 'octopus'){?> selected="selected" <?php } ?> value="octopus">Miss Octoman/Octopus</option>
		</select>
		<button class="start-btn" data-start>Start</button>
	</div>
</div>

<main>
	<div class="site-width">
	
		<div class="day" id="day-1" style=" display:<?php echo (session_page() == 1) ? 'block' : 'none'; ?>;">
			<?php require_once(__DIR__ . '/days/1.php');?>
		</div>
		<div class="day" id="day-2" style=" display:<?php echo (session_page() == 2) ? 'block' : 'none'; ?>;">
			<?php require_once(__DIR__ . '/days/2.php');?>
		</div>
		<div class="day" id="day-3" style=" display:<?php echo (session_page() == 3) ? 'block' : 'none'; ?>;">
			<?php require_once(__DIR__ . '/days/3.php');?>
		</div>
		<div class="day" id="day-4" style=" display:<?php echo (session_page() == 4) ? 'block' : 'none'; ?>;">
			<?php require_once(__DIR__ . '/days/4.php');?>
		</div>
		<div class="day" id="day-5" style=" display:<?php echo (session_page() == 5) ? 'block' : 'none'; ?>;">
			<?php require_once(__DIR__ . '/days/5.php'); ?>
		</div>
		
		<button data-continue class="continue-btn">Continue</button>
		

		
		
	</div>
	
</main>




<?php require_once(__DIR__ . '/system/inc/foot.inc.php');?>