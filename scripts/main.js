;(function($, window, document, undefined){
	
	
	$(function(){
		initMenu();
		initTeacher();
	});
	
	function initMenu(){
		$("body").on("change", '#day-picker' ,function(){
			var $picker = $(this);
			$(".day").hide();
			$(".role-script-block, .direction-block, .continue-btn").hide();
			$("#day-" + $picker.val()).show();
			$.post('/api.php', {
				api_function : 'update_session',
				key : 'page',
				value : $picker.val()
			});
		});
		$("body").on("change", '#role-picker', function(){
			var $picker = $(this);
			window.master_role = $picker.val();
			$.post('/api.php', {
				api_function : 'update_session',
				key : 'role',
				value : $picker.val()
			});
		});
	}
	
	function initTeacher(){
		$("body").on("click", "[data-start]", function(){
			$(".role-script-block").hide();
			$(".continue-btn").fadeIn(300);
			teacherProgress('start');
		});
		$("body").on("click", "[data-continue]", function(){
			teacherProgress('continue');
		});
	}
	
	function teacherProgress(btn){
		var btn = typeof btn !== 'undefined' ? btn : 'start';
		var counter = 0;
		$(".day:visible [data-role]:hidden").each(function(){
			var $block = $(this);
			var block_role = $block.attr("data-role").split(',');
			if (block_role.indexOf(master_role) < 0 || counter == 0){
				$block.addClass('on');
				var delay = (btn == 'start' || counter == 0) ? 0 : 400;
				var timeout = setTimeout(function(){
					$block.fadeIn(300);
					if (btn != 'start'){
						$("html, body").stop().animate({ scrollTop: $(document).height() }, 300);
					}
				}, (delay*counter));
				counter++;
			}else{
				return false;
			}
			
			if ($(".day:visible [data-role*='"+master_role+"']:not(.on)").length == 0){
				$(".day:visible [data-role*='"+master_role+"']").fadeIn(300);
				
				if (master_role != ''){
					var timeout = setTimeout(function(){
						alert("There aren't any more lines for chosen role.");
						$(".day:visible [data-role]:hidden").fadeIn(300);
					}, 2000);
				}else{
					$(".day:visible [data-role]:hidden").fadeIn(300);
				}
				
				
				return false;
			}
		});
	}
	
	
	
})(jQuery, window, document);