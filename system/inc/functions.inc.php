<?php



function update_session(){
	$key = $_POST['key'];
	$value = $_POST['value'];
	$_SESSION[$key] = $value;
}

function session_page(){
	if (isset($_SESSION['page'])){
		return $_SESSION['page'];
	}
	return 0;
}

function session_role(){
	if (isset($_SESSION['role'])){
		return $_SESSION['role'];
	}
	return NULL;
}