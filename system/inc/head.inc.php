<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">


<link href='/styles/main.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css'><?php

echo csscrush_tag( '/styles/main.css', array('output_dir' => __DIR__ . '/../../styles/css_crush', 'source_map' => true));?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/main.js"></script>

<script type="text/javascript">
window.master_role = <?php echo !is_null(session_role()) ? "'".session_role()."'" : 'null'; ?>;
</script>


<title>Script Helper</title>
</head>

<body>