
<div class="direction-block">
	<p><strong>DAY 2 DRAMA</strong></p>
	<p>Characters: Scuba-Man, Snorkel-Boy, Great White, Octopus, Turtle, Alex.</p>
	<p>(Day 2 Recorded Opening)</p>
	<p>(Just outside the Scuba-Cave, the SUPERHEROES are looking at their Deception Detector.)</p>
</div>


<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">It's much worse than I thought, Scuba-Man.  When I scan the entire world, the Deception Detector goes berserk! Look at this! Fraud, cheating, counterfeiting, lying, dishonesty ... I can't stand it! Evil is everywhere!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">It's not surprising, Snorkel-Boy. The Master Deceiver and his evil minions have been working overtime since that first deception in the Garden of Eden. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Kinda reminds me of what it must have been like in the days of Noah before the Flood when everyone's thoughts were only evil continually. It makes me shudder to think about it! <div class="inline-direction">(as he shudders)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Yeah, that was one wicked world, all right ... except for Noah, of course! The only person on Earth who still loved and obeyed God. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Which makes me thankful for the Bible. What a gift to have The Word with us all the time so, with God's help, we can know how to walk with God like Noah did.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Tis true ... but sadly, many still choose to ignore God's Word and go their own way ... and so we have to keep watch and be ready.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Right ... <div class="inline-direction">(then an alert [sfx] sounds and he looks back at the screen)</div> Look! Something's happening at the Grand Canyon. It's a tour guide. I'll open the sound port so we can hear what he's saying.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Tour Guide</div>
	<div class="line"><div class="inline-direction">(Recorded Voice)</div> " ... you see this layer of rock. This is estimated to be 330 million years old. And how do we know this? Very simple. Because each layer has what is called an index fossil . . . ."</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Slithering Sea Serpent, Scuba-Man! He's using circular reasoning!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Precisely, Boy-Wonder! They use the fossils to date the rocks and the rocks to date the fossils. It's one of the Enemy's most effective tricks. This "millions of years" deception has got the whole world in a headlock! <div class="inline-direction">(as another alert [sfx] sounds, he notices a message on screen)</div> Wait! What's that? </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Oh, it's just a pop-up ad. Those things are so annoying. I'll get rid of it.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">No, wait! It's a distress call!  
Snorkel Boy: "HELP! I'VE FALLEN, AND I CAN'T GET UP!  YOUR FRIEND, ALEX."  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">I knew we shouldn't have let her just wander off!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">But how will we find her? She could be anywhere! <div class="inline-direction">(Another alert [sfx] sounds, then he notices that there's more to the message.)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Wait, there's more! "P. S. YOU CAN FIND ME AT PORPOISE COVE ... AND PLEASE HURRY!"   </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">That's strange. How did she know how to communicate with us? Something seems "fishy" about this, Scuba-Man.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Nevertheless, we shouldn't take any chances.  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">To the Scuba-Sub?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">You read my mind, Boy-Wonder!</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(The SUPERHEROES quickly drop what they're doing and go straight to the Scuba-Sub which is parked close by.)</p>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(as they're getting into the Scuba-Sub)</div> Scuba-Man? Do you think you could just call me Snorkel-Boy from now on? I mean, really, that "Boy-Wonder" stuff ... it's kind of embarrassing.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Well, all right ... if that's how you feel. I didn't realize you were so sensitive. <div class="inline-direction">(pause)</div> You know, Batman used it, and I just thought ... oh, never mind ... c'mon, let's go! <div class="inline-direction">(tries to start the Scuba-Sub and gets a dead battery sound [sfx].)</div>  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Deep Sea Disaster, Scuba-Man! The battery's dead! Now what do we do?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">There's only one thing to do. You get the jumper cables, and I'll get the electric eel.</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(They run as fast as they can into the Scuba-Cave. Meanwhile, GREAT WHITE and OCTOPUS emerge briefly from their hiding spot near the cave.)</p>
</div>			
<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Yes! Our plan is working! As soon as they get their little sub fixed, they'll be on their way to Porpoise Cove where a little surprise will be waiting for them! Ha, ha, ha! </div>
	<div class="clearfix"></div>
</div>


<div class="role-script-block">
	<div class="role">GREAT WHITE</div>
	<div class="line">And that will be the end of the simpleminded superheroes!</div>
	<div class="clearfix"></div>
</div>
	
<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Then we'll find Alex and rescue her from that BIBLE of theirs! <div class="inline-direction">(Then GREAT WHITE and OCTOPUS duck away as the SUPERHEROES return with jumper cables and eel in hand.)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(with the jumper cables)</div> You know, it's been a while since I've done this. Is it "Stop, Drop, and Roll?"  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Uh ... no. That's what you do if you're on fire. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Oh, yeah. Here, you do it ... I don't trust myself. <div class="inline-direction">(as he gives the jumper cables to SCUBA-MAN)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Okay, now watch closely. <div class="inline-direction">(as he pretends to hook up the cables)</div> See ... you start with the dead battery and go positive to positive and negative to negative. Now, start the Scuba-Sub, then I'll remove the cables.</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(SNORKEL BOY quickly climbs in and starts the Scuba-Sub [sfx], then SCUBA-MAN removes the cables and gets in.)</p>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">TO INFINITY AND BEYOND!!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">What was that?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(feeling stupid)</div> Uh ... forget it ... wasn't important.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">TO PORPOISE COVE!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">TO PORPOISE COVE!</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(The SUPERHEROES, in the Scuba-Sub with engine running [sfx], drive up the side aisle and out the back doors of the auditorium as GREAT WHITE and OCTOPUS come out of hiding.)</p>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">You'd better make the call.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Right. <div class="inline-direction">(as she pulls out her cell phone)</div> Hey, it's me. The Terrible Twosome just left, so be ready.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">And tell them not to mess up this time!</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">And Great White says you'd better not mess up this time! <div class="inline-direction">(pause, then to GREAT WHITE)</div> They said not to worry.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Hey, it's they that should be worried, not me. <div class="inline-direction">(short pause)</div> I don't get worried ... I get hungry. <div class="inline-direction">(TURTLE, with her messenger bag, enters at the opposite side of the stage.)</div></div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(a bit unnerved by GREAT WHITE'S hungry look)</div> You know, I'll think I'll stand over here <div class="inline-direction">(as she moves closer to center stage, then notices TURTLE)</div> Uh-oh ... we've got company.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Hmm ... looks more like lunch to me. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Oh, hello! I didn't know we had visitors! I'm Turtle, and I'm with the Ocean Land Welcome Committee. You're new here, aren't you?</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(quickly makes something up)</div> Uh ... yeah ...  just arrived from the Gulf of Mexico.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Really! I've got family just off the coast of Florida! I absolutely love it up there. Ever been to Sea World? </div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">No ... can't say that I have. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Well, if you ever want to go, let me know because I've got some great discount tickets. In the meantime, here's a visitor information packet. <div class="inline-direction">(as she hands OCTOPUS a visitor packet)</div> It's got info about everything to see and do here in Ocean Land, including a map, a calendar of events, and money-saving coupons.</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(insincere)</div> Thanks.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Oh, you're most welcome. Do you have any questions? </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Yeah ... where can I get something to eat? I'm starving.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(suddenly a bit uneasy)</div> My ... what big ... teeth you have! </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">All the better to eat — <div class="inline-direction">(OCTOPUS motions to him suddenly)</div> uh ...  nevermind. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(short pause)</div> Uh ... well ... we have some excellent restaurants. What are you, uh ... hungry for? </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">How 'bout ... turtle soup? <div class="inline-direction">(as he steps closer to TURTLE)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(scared)</div> Tur ... tur ... tur ... turtle soup?? Haha ... can I interest you in a turtle sundae, instead? A new creamery just opened around the corner, and it's getting rave reviews. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Hmm ... interesting idea. I've never eaten dessert first, but ice cream does sound pretty good right now. I'll be right back. Don't go anywhere.  <div class="inline-direction">(GREAT WHITE exits.)</div></div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">You know ... I don't mean to be giving you advice, but you might want to leave before he comes back. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Funny ... I was just thinking the same thing. Thanks for the tip.</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Don't mention it. <div class="inline-direction">(TURTLE quickly exits, then ALEX enters from the other side of the stage.)</div></div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Now to find Alex. Oh, splendid! Here she comes now. <div class="inline-direction">(ALEX "leafs through" the pages of her Bible as she slowly walks toward center stage. She doesn't notice OCTOPUS as she finds a place to sit near the front of the Scuba-Cave.)</div></div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(remaining perfectly still)</div> Well, hello there, Alex.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(startled, she quickly stands up)</div> Who said that?</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">I did <div class="inline-direction">("unfreezes," moves a step or two towards ALEX)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(scared, moves back a step or two)</div> What do you want? And how did you know my name??</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(quickly thinks of an answer)</div> Uh ... Turtle told me. Yeah, that's right, Turtle told me.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You're friends with Turtle??</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Of course! We've been buddies for, uh ... well ... a long time. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You're not very convincing.  </div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">You know, Alex ... <div class="inline-direction">(as she steps closer)</div> I'd really like to be your friend.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(ALEX steps back)</div> I don't know. I've never been friends with an Octopus before. And besides, I'm not so sure that I trust you. </div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(very assuring)</div> Oh, you can trust me.  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(uneasy, gets an idea)</div> You, know ...  I think I should go find Turtle. </div>
	<div class="clearfix"></div>
</div> 

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Oh no, you shouldn't do that ... I mean ... <div class="inline-direction">(stalling)</div> especially at a time like this.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">A time like what?</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(making it up as she goes)</div> Well ... um ... it's ... it's Shell Inspection Season. Didn't you know that? </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Shell Inspection Season?? </div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Why, yes. Every year at this time all the turtles in the whole world are frantically getting ready to go to a certain place far, far away to have their shells inspected. Didn't you watch Finding Nemo?  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah.</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Well, where did you think all those turtles were going?? Now listen, Alex. We're wasting precious time when we could be getting to know each other. <div class="inline-direction">(notices ALEX'S Bible)</div> What's that book you have?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">This? <div class="inline-direction">(holds it up)</div> It's the Bible.</div>
	<div class="clearfix"></div>
</div>  

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Scuba-Man gave it to you, didn't he?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(surprised)</div> Yeah, how'd you know?</div>
	<div class="clearfix"></div>
</div>
	
<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Oh, he gives those out to everyone. Says it's a guidebook or something. I don't know what the big deal is. I surely wouldn't want one if I were you.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(curious)</div> Really, why not?</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Simple. You just don't need it, girl! You've got common sense don't you??</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Well, yeah ... I guess.</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">And if you don't know something, all you have to do is find someone who does ... like me! <div class="inline-direction">(OCTOPUS puts arm around ALEX'S shoulders)</div> I can tell you anything you need to know. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You can?</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Absolutely. Here ... let me get rid of this for you <div class="inline-direction">(as she takes the Bible from ALEX'S hand)</div>. You know what, Alex ... I think we're going to be really, really good friends.</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(Suddenly, the Scuba-Sub [sfx] is heard in the distance. The SUPERHEROES are returning from their "wild goose chase.")</p>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What's that sound??</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(quickly becomes alarmed)</div> What?! It couldn't be! </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What's wrong? </div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Uh ... you know ... I ... I ... just forgot. I have a ... a manicure this afternoon, and I'm late. Sorry, Alex, we'll have to do it another time.   <div class="inline-direction">(OCTOPUS drops ALEX'S Bible and runs off stage.)</div></div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">But, wait! When will you come back?? <div class="inline-direction">(pause)</div> That was strange.</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(Suddenly the SUPERHEROES, in the Scuba-Sub, burst into the Auditorium through the rear doors and proceed down the side aisle toward the stage. ALEX sees them coming and is fascinated by their vehicle.)</p>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(walks over to see the Scuba-Sub)</div> Wow! Now THAT is awesome!  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(as he powers down the Scuba-Sub [sfx])</div> This? Oh, yeah ... it's a Z007GTX Turbo with power-everything, cruise-control, navigation, back-up camera ... you know, the whole package.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Except Wi-Fi. That was an add-on.  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Could you take me for a ride?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Well, I don't know, Alex. We don't usually ... .<div class="inline-direction">(Suddenly the SUPERHEROES realize simultaneously who they're talking to.)</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba-Man and Snorkel-Boy</div>
	<div class="line"><div class="inline-direction">(together)</div> ALEX???!! </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Are you OK?? Are you hurt?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(confused)</div> Of course, I'm OK. Why wouldn't I be??</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">You've had us quite worried, young lady.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">How'd you get back from Porpoise Cove?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Porpoise Cove? What are you talking about?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">You mean ... you weren't at Porpoise Cove?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Wild underwater goose chase, Scuba-Man! I just knew there was something "fishy" going on!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(notices a Bible on the floor)</div> What's this? A Bible! Is this yours, Alex?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah ... </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Why is it on the ground?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(as he picks it up and hands it to ALEX)</div> Alex, I don't think you realize how important this book is. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">It's actually the very words of God. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">That's right. And it contains God's plan of salvation. Something you can't live without. </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">But I've got common sense and ... friends to help me when I need it.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Great tidal wave of confusion, Scuba-Man! Sounds just like the Enemy! If there isn't a dangerous deception going on here, my name isn't Snorkel-Boy!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Precisely my thought, Boy-Wonder! Oops ... sorry. <div class="inline-direction">(short pause)</div> Well, anyway, we need to find out where the deception is coming from! <div class="inline-direction">(turns to ALEX)</div> Alex, I'm going to insist that you stay inside the Scuba-Cave until we make sense of this. Snorkel-Boy will escort you. I'm going to run ahead and get started on the case.</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(SCUBA-MAN exits into the Scuba-Cave.)</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(excited, turns to SNORKEL-BOY)</div> Really? You mean I get to see inside the Scuba-Cave??? </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well, it's not exactly what you're thinking. You'll be in the Protection Chamber ... but you won't have access to the Inner Cave.   </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(disappointed)</div> So I won't get to see all your secret stuff?  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Sorry ... <div class="inline-direction">(reassuring)</div> but don't worry, the Protection Chamber is clean and comfortable, and it comes with a deluxe Continental Breakfast!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(sarcastic)</div> Gee, I can hardly wait. So, how long do I have to stay in there?</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Only until we can detect and diffuse this dangerous deception. Really, Alex, it's for your own safety. Try to understand. C'mon, let's go.</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(realizing resistance is futile, she thinks up an escape plan)</div> You know, you're right. This is no place for a kid to be wandering around by herself.  <div class="inline-direction">(pretends to notice shoelace is untied, bends down)</div> Oh ... I ...  just noticed my shoelace is untied. You go on ahead, I'll catch up with you in a second.  </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well ... okay. But hurry!</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(As soon as SNORKEL-BOY disappears into the Scuba-Cave, ALEX tip-toes, then runs away. Soon SNORKEL-BOY emerges from the cave looking for ALEX.)</p>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Alex? <div class="inline-direction">(pause)</div> Alex? <div class="inline-direction">(pause as he thinks and talks to himself)</div> Where'd she go? Great kettle of fish! She must have run away! <div class="inline-direction">(he ducks into the Scuba-Cave and calls SCUBA-MAN)</div> Scuba-Man!! <div class="inline-direction">(Then SCUBA-MAN emerges from the Scuba-Cave.)</div> </div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Alex is gone! I can't believe I fell for the old "my shoelace is untied" trick! She was wearing flip flops!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">We can't worry about that now, Boy-Wonder!  ... Sorry ...  We've got to find Alex before someone or something else does! You go that way <div class="inline-direction">(points to side aisle)</div>, and I'll go this way <div class="inline-direction">(points to other side aisle)</div>. She couldn't have gotten very far!</div>
	<div class="clearfix"></div>
</div>
<div class="direction-block">
	<p>(SCUBA-MAN and SNORKEL BOY run out the back of the Auditorium.)</p>
</div>


<div class="direction-block">
	<p>(Day 2 Recorded Closing)</p>
</div>	