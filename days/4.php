
<div data-role="0" class="direction-block">
	<h1>Day 4</h1>
	<p>Characters: Alex, Scuba-Man, Snorkel-Boy, Turtle, Octopus, Great White.</p>
	<p>(On the way to The Abyss, SCUBA-MAN and SNORKEL-BOY overpowered GREAT WHITE and OCTOPUS. In the struggle, GREAT WHITE got away, but OCTOPUS was captured and "hand-cuffed." The SUPERHEROES are now en route to the Scuba-Cave with OCTOPUS.)</p>
	
	<p>(Day 4 Recorded Opening)</p>
	<p>(The scene begins with GREAT WHITE quickly walking across the stage to the Scuba-Cave. He's arranged an ambush, with some of his thugs, to take place inside the Scuba-Cave as soon as the SUPERHEROES arrive.)</p>
</div>


<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">I have to commend you, Snorkel-Boy, for having the Octo-cuffs with you. I'm not sure what we would have done without them. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well, like I said before, Scuba-Man, it pays to be prepared. <div class="inline-direction">(then notices the "Happy Birthday" sign)</div> Great natal anniversary surprise, Scuba-Man! Someone's remembered your birthday! </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Yes ... I see that, Snorkel-Boy! And naturally, the question that follows is ... who? Who could have done this?</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Who cares? You got cake, didn't you?</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Yes, but he doesn't know who to thank for such a kind gesture.</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Oh, brother. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Of course, we wouldn't expect a villain such as yourself to understand the need to express gratitude. But know that being thankful is an extremely important virtue, and one that God expects from all of us. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Yeah, Octopus, 1 Thessalonians 5:18 says, "In everything, give thanks; for this is the will of God in Christ Jesus for you."</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">And how could we be anything but thankful for all He's done? It was an incredibly costly thing what Jesus did—taking the punishment for our sins so we could have peace with God. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Yeah, and all God requires is that we accept his gift in faith, that we believe Jesus is the Son of God who came to be our savior, and that he took our punishment for sin when he died on the cross.  Talk about an incredible gift!</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Okay, okay ... sorry I said anything! <div class="inline-direction">(SCUBA-MAN takes a closer look at the cake.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Mmm ... As much as I'd like to dive right into this cake, we must exercise self-control and secure the prisoner first. You stay with Octopus while I go into the Scuba-Cave and prepare the Evil Prisoner Detention Cell. I'll be back momentarily.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">You can count on me, Scuba-Man. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(suddenly hears something from inside the cave)</div> Wait! My super-sensitive hearing is picking up some movement inside the cave. I think we may have some visitors ... and I don't mean the welcome kind.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Jumping jellyfish, Scuba-Man! If someone is waiting to attack, you're going to need my help! </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Negative, Snorkel-Boy! You MUST stay with the prisoner!</div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce" class="direction-block">
	<p>(SCUBA-MAN leaves and goes into the Scuba-Cave.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(admiring)</div> Man, I hope to be like him someday!</div>
	<div class="clearfix"></div>
</div>

<div data-role="0" class="direction-block">
	<p>(Then, suddenly, a fight [sfx and video] ensues inside the cave with various sound effects and articles flying out of the cave.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Scuba-Man! Are you okay?!</div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce,white" class="direction-block">
	<p>(Then SCUBA-MAN emerges from the cave with GREAT WHITE wrapped in chain and padlocked.)</p>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Look what I caught!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Great White Shark, Scuba-Man! You got him!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Now to prepare Evil Prisoner Detention Cells A and B for these two. Watch them closely ... I won't be long.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Don't worry ...  I won't let them out of my sight.  <div class="inline-direction">(SCUBA-MAN exits into the Scuba-Cave.)</div></div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Wow! I'm impressed. No wonder they call you The Dynamic Duo of the Deep. You guys are really special.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Aw, shucks, Octopus. You probably say that to all the superheroes.</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Nonsense, Boy-Wonder! I mean look at what you did back at The Abyss. Great White and I thought we had the upper hand, but you sure proved us wrong!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Yeah, that "pretending to be asleep" routine really worked. How did you do that? </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(as he stretches his arms and then places his hands behind his head, exposing his armpits next to GREAT WHITE)</div> Oh, well we just developed an immunity to Sleep Walking Spray, that's all. We did the same thing with Kryptonite.</div>
	<div class="clearfix"></div>
</div> 

<div data-role="bruce" class="direction-block">
	<p>(SCUBA-MAN comes out of the Scuba-Cave.)</p>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Ok, the cells are ready. Octopus and Great White, you're both under arrest for conspiring to silence the Word of God. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Not to mention your devious plot to do away with Scuba-Man and Snorkel-Boy!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">And it would've worked too, if it weren't for those meddling kids!</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(confused)</div> What?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">I've just always wanted to say that.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">C'mon, let's go.</div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce,wayne,alex" class="direction-block">
	<p>(The SUPERHEROES escort OCTOPUS and GREAT WHITE into the Scuba-Cave. Then ALEX, still carrying her Bible, enters from side stage—from where she had left to rescue the SUPER­HEROES. She walks slowly to center stage and sits down a few feet in front of the Scuba-Cave.)</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(sad, depressed, wimpers, sniffles)</div> I can't believe it. They were just trying to help me. <div class="inline-direction">(pause)</div> They were just trying to help me and now ...  <div class="inline-direction">(sniff, sniff)</div> they're gone forever.</div>
	<div class="clearfix"></div>
</div>

<div data-role="alex,wayne" class="direction-block">
	<p>(Hearing ALEX cry, SNORKEL-BOY emerges slowly from the Scuba-Cave.)</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(doesn't know SNORKEL-BOY is nearby)</div> They're gone forever ... and it's all my fault! <div class="inline-direction">(pause)</div> Ocean Land will never be the same now. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(while half-crying)</div> Gee, Alex ...  I'm so sorry to hear about your friends. Is there anything we can do? </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">No ... there's nothing anyone can—<div class="inline-direction">(then realizes it's SNORKEL-BOY)</div> Snorkel-Boy?? SNORKEL-BOY!! I can't believe it! How'd you get here??</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">What do you mean? ... I just walked.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">But ... what about The Abyss??</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">The Abyss?  </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Octopus and the shark took you and Scuba-Man away!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Oh, that was just an act. We weren't really asleep.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(excited, happy)</div> I can't believe it! I thought that was the end of you! But you're okay!</div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce" class="direction-block">
	<p>(SCUBA-MAN emerges from the Scuba-Cave.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Yeah ... I think so. <div class="inline-direction">(as he looks himself over)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(to ALEX)</div> There you are. You know, you've caused us a lot of concern young lady.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">I know, and I'm really sorry. You were just trying to protect me, and I was too immature to see that. And Turtle tried to warn me about "stranger danger," but I was too busy and excited to pay attention. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Oh, well. There's no real harm done.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">And at least you learned something.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah ... I learned that I've got a lot of growing up to do, AND that I need to take this book more seriously. <div class="inline-direction">(as she holds up her Bible)</div> </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Well ... then it was all worth it. <div class="inline-direction">(Then TURTLE pokes her head out.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Pssssst!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What was that?</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Oh, it's Turtle. <div class="inline-direction">(to TURTLE)</div> What is it?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Is the big shark gone?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(to SNORKEL-BOY)</div> She probably means Great White. <div class="inline-direction">(to TURTLE)</div> Yes, he's in custody. I just locked him up a minute ago.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">So it's safe to come out?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Yes, it's safe.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Whew! That's good. For a while there, I thought for sure I was going to be an appetizer!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Speaking of food ... I'm getting hungry. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">I could go for some nourishment, as well. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Well, good, because I've got your birthday dinner ready and waiting. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(to SCUBA-MAN)</div> Oh ... so it was Turtle who put up your birthday decorations! </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Well, of course! I'm the one who keeps the Ocean Land calendar, you know.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Thank you. That was very kind.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well, what are we waiting for?! Let's eat!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Can I carry the cake?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Sure.</div>
	<div class="clearfix"></div>
</div>


<div data-role="turtle,bruce" class="direction-block">
	<p>(Some exit, followed by SCUBA-MAN, and apparently everyone else, cause they are lazy memorizers.)</p>
</div>
<div data-role="turtle,white" class="direction-block">
	<p>(TURTLE exits, then we hear GREAT WHITE talk about cake.)</p>
	<p>(Day 4 Recorded Closing)</p>
</div>	