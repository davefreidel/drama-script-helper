
<div data-role="0" class="direction-block">
	<h1>Day 1</h1>
	<p><strong>Characters: Alex, Bruce (Scuba-Man), Wayne (Snorkel-Boy), Mom (Turtle), Mr. White (Great White), Miss Octoman (Octopus).</strong></p>
	<p>(Theme music)</p>
</div>		

<div data-role="wayne,bruce,alex" class="direction-block">
	<p>(As WAYNE exits, BRUCE returns to center stage where his "home" is.  He walks into his living room where his daughter, ALEX, is seated and looking at a catalog.)</p>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Hey, Alex. What are you reading?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Oh ... we were in this really cool Aquarium store today, called Ocean Land, and I picked up a catalog.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line"><div class="inline-direction">(curious)</div> Really? Let me see ... <div class="inline-direction">(ALEX hands BRUCE the catalog)</div> Wow! I had an aquarium when I was young ... but nothing like these. I didn't know you liked fish <div class="inline-direction">(as he hands the catalog back to ALEX)</div>.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Well, I do now ... look at this one. It's a clownfish. I wonder why they call it a clownfish.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">I don't know. Maybe 'cause it's always joking around. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(rolls her eyes)</div> Dad!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Oh, c'mon. Don't be such a "crab!"</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Dad!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Okay, okay ... I'm sorry! I didn't do it on "porpoise!" </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(laughs)</div> Now that was bad! <div class="inline-direction">(changes subject)</div> Hey, why'd you get home so late? Important meeting or something?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Sort of. Just as I was getting ready to leave, my boss called Mr. Wayne and me into his office ... and, uh ...  well, he wasn't very happy.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Really?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Yeah ... I guess you could say we "got called into the principal's office."</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="direction-block">
	<p>(Then MOM enters the room.)</p>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">So ... how'd your meeting go?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Dad got in trouble.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Really? What happened?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Well ... Mr. Wayne and I had been talking with some of our co-workers about the Grand Canyon and how there's a much better explanation than the Colorado River and millions of years. We explained that Noah's Flood makes more sense that the Grand Canyon could have been carved in a matter of days or weeks.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">And let me guess ... Mr. White didn't like that.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Not one bit.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Oh, boy. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">We're just going to keep being "a light in the darkness" and leave the results to Him. But, hey ...  let's talk about something else.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line"><div class="inline-direction">(to BRUCE)</div> Well, I need to get you something to eat <div class="inline-direction">(as she starts to exit)</div>. And by the way, have you decided what you want for your birthday dinner tomorrow night? </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">As long as there's cake, it doesn't matter to me!</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Okay. It's a deal.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="direction-block">
	<p>(MOM exits the room.)</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(with a smile)</div> You and your cake.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">I know ... I'm hopeless. <div class="inline-direction">(then he pulls a small Bible out of his coat pocket)</div> Recognize this? Found it in my car.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">My Bible ... oops.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">I guess it's been in there since Sunday, right?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Sorry.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Listen, Alex ... reading the Bible just to please your parents is not what this is about. No, you have to be convinced in your own mind of how important it is. I mean, think about what you have here—a copy of the very words of God <div class="inline-direction">(as he opens it)</div>—and in it, the good news about Jesus, the only way for us to be saved and go to heaven. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Hmm ... I've never thought of it that way. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">But I'm not trying to make you feel guilty. I just want to make sure you're taking it seriously. <div class="inline-direction">(he hands her the Bible)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(respectful)</div> I understand.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Hey, you look really tired. Maybe you should go to bed early tonight. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">I will ... as soon as I finish this catalog. <div class="inline-direction">(speaking while yawning)</div> I promise.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Bruce</div>
	<div class="line">Yeah, right ... more than likely we're gonna find you asleep dreaming about tropical fish and aquarium gravel! <div class="inline-direction">(pause as he sniffs the air)</div> Oooo ... time for me to exit. I smell food! </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(she closes the catalog)</div> Okay, Alex, time to wake up. <div class="inline-direction">(she opens the catalog again, then moves to the floor and continues reading; soon her head starts to droop)</div> I'm just gonna close my eyes for a couple minutes ... that's all. <div class="inline-direction">(then she falls asleep)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="0" class="direction-block">
	<p><strong>SCENE TWO</strong></p>
	<p>(QUICK SCENE CHANGE:  Stage lights down. While ALEX "sleeps," stagehands quickly remove the living room pieces as dream music [sfx] and screen effects communicate a dream scene. ALEX is now dreaming about the ocean. Then, stage lights come up as TURTLE, with a messenger bag, enters the scene and notices ALEX lying motionless. Curious, TURTLE decides to check out this unusual sight.)</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(sleepily responds as she senses TURTLE)</div> I'm in bed, Mom. Sorry I didn't say good-night. I jus ... <div class="inline-direction">(yawns)</div> ... I just got so sleepy. <div class="inline-direction">(turns over)</div> See you in the morning. <div class="inline-direction">(then her eyes open briefly enough to see TURTLE staring her in the face.)</div> </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Are you a fish??</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What??</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">I said ... are YOU ...  a fish?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">A fish?? Of course I'm not a fish. I'm ... I'm Alex. I'm a ... I'm a girl!</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">A girl? You mean, you're human? I didn't know humans could breathe under water.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(looking around)</div> Well ...  I didn't know turtles could talk!  ... <div class="inline-direction">(suddenly realizing what TURTLE said)</div> Wait, did you say, "under water??" Where am I?? <div class="inline-direction">(confusion turns to amazement)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">You don't know? Why you're in Ocean Land, of course! </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(distracted by all she sees)</div> Ocean Land of course??</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">No, No, No.  Not "Ocean Land of course," just plain Ocean Land.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Wow ... doesn't look plain to me! <div class="inline-direction">(starts walking around to see everything)</div> This place is beautiful! Almost seems like a ... like a dream or something! </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Well, I'm glad you like it. But I must warn you that even though Ocean Land is a beautiful place, not everyone down here is your friend and you mustn't go near the Abyss. It's a bottomless pit, and if you fall into it, you'll be lost forever.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Stay clear of the Abyss and strangers. Boy, you're starting to sound like my mom. Got it.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="direction-block">
	<p>(Meanwhile, ALEX has noticed a big red button near the mouth of the Scuba-Cave.)</p>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Wait! No, Alex! Don't touch that!<div class="inline-direction">(ALEX, overcome with curiosity, pushes the button and an emergency alarm [sfx] sounds. Then SCUBA-MAN emerges from the Scuba-Cave.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(saluting ALEX)</div> The Dynamic Duo of the Deep at your service! <div class="inline-direction">(then he notices that SNORKEL-BOY is missing)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(yells from inside the Scuba-Cave)</div> I'll be right there! </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(to himself)</div> Well, this is embarrassing. <div class="inline-direction">(Then SNORKEL-BOY runs out of the cave while tying his cape on.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Sorry, I couldn't find my cape.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(to SNORKEL-BOY)</div> Nevermind about that now. <div class="inline-direction">(to ALEX)</div> Is there a problem, young lady?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Who me? No ... I just pushed that big red button, that's all.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Sounds like the old practical joke of "knock, knock, and run," Scuba-Man! We may have a prankster on our hands!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Not so fast, Snorkel-Boy, I'm sensing that it wasn't intentional.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(stepping forward)</div> Of course it wasn't intentional, Scuba-Man. She was just curious. This is Alex ... she's new here. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(looks ALEX over)</div> Well, hello, Alex. Welcome to Ocean Land! This is Snorkel-Boy and I'm Scuba-Man.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(to ALEX)</div> They're, uh ... superheroes. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">That's right, Alex. And it's our job to be a light in the darkness ...  to keep watch, stand fast in the faith, be brave and be strong. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">That's from 1 Corinthians 16:13, in case you're wondering. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">So, Alex, in our stand for truth, we give these out. <div class="inline-direction">(as he hands ALEX a Bible)</div> Some call it "The Word" and some call it "The Book." In Ocean Land, we refer to it as "The Bible." </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">And because it's the Word of God, we can trust everything in it to be the truth, the whole truth, and nothing but the truth. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">It's an incredible treasure, Alex ... if you'll read it and follow it.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Hmm ... <div class="inline-direction">(as she looks it over then notices "OSV" on the spine)</div> What's OSV?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Ocean Standard Version. Snorkel-Boy, did we miss anything?</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Just one thing ...  <div class="inline-direction">(to ALEX)</div> Alex, the big red button <div class="inline-direction">(pointing to the big red button)</div> is not a doorbell. It's for emergencies only, please. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Sorry. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Just be careful and remember what I told you. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">I will. And thanks for everything! <div class="inline-direction">(as she starts to walk away, then pauses)</div> You know, it's strange, but you all look familiar to me for some reason. <div class="inline-direction">(then shrugs her shoulders)</div>  ... oh, well. <div class="inline-direction">(ALEX ventures off as TURTLE and SUPERHEROES watch. Meanwhile, OCTOPUS and GREAT WHITE sneak away in the opposite direction.)</div> </div>
	<div class="clearfix"></div>
</div>

<div data-role="0" class="direction-block">
	<p>(Day 1 Recorded Closing)</p>
</div>