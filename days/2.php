
<div data-role="0" class="direction-block">
	<h1>Day 2</h1>
	<p>Characters: Scuba-Man, Snorkel-Boy, Great White, Octopus, Turtle, Alex.</p>
	<p>(Day 2 Recorded Opening)</p>
	<p>(Just outside the Scuba-Cave, the SUPERHEROES are throwing the porpoise.)</p>
</div>


<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Slithering Sea Serpent, Scuba-Man! It's a distress call! "HELP! I'VE FALLEN, AND I CAN'T GET UP!  YOUR FRIEND, ALEX."  </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">I knew we shouldn't have let her just wander off!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">But how will we find her? She could be anywhere! <div class="inline-direction">(Another alert [sfx] sounds, then he notices that there's more to the message.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Wait, there's more! "P. S. YOU CAN FIND ME AT PORPOISE COVE ... AND PLEASE HURRY!"   </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">That's strange. How did she know how to communicate with us? Something seems "fishy" about this, Scuba-Man.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Nevertheless, we shouldn't take any chances.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">To the Scuba-Sub?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">You read my mind, Boy-Wonder!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne" class="direction-block">
	<p>(The SUPERHEROES quickly drop what they're doing and go straight to the Scuba-Sub which is parked close by.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(as they're getting into the Scuba-Sub)</div> Scuba-Man? Do you think you could just call me Snorkel-Boy from now on? I mean, really, that "Boy-Wonder" stuff ... it's kind of embarrassing.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Well, all right ... if that's how you feel. I didn't realize you were so sensitive. <div class="inline-direction">(pause)</div> You know, Batman used it, and I just thought ... oh, never mind ... c'mon, let's go! <div class="inline-direction">(tries to start the Scuba-Sub and gets a dead battery sound [sfx].)</div>  </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">TO PORPOISE COVE!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">TO PORPOISE COVE!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne,white,octopus" class="direction-block">
	<p>(The SUPERHEROES, in the Scuba-Sub with engine running [sfx], drive up the side aisle and out the back doors of the auditorium as GREAT WHITE and OCTOPUS come out of hiding.)</p>
</div>			
<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Yes! Our plan is working! As soon as they get to Porpoise Cove where a little surprise will be waiting for them! Ha, ha, ha! </div>
	<div class="clearfix"></div>
</div>


<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">And that will be the end of the simpleminded superheroes!</div>
	<div class="clearfix"></div>
</div>
	
<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Then we'll find Alex and rescue her from that BIBLE of theirs!</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle,alex" class="direction-block">
	<p>(ALEX enters from the other side of the stage.)</p>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Now to find Alex. Oh, splendid! Here she comes now. <div class="inline-direction">(ALEX "leafs through" the pages of her Bible as she slowly walks toward center stage. She doesn't notice OCTOPUS as she finds a place to sit near the front of the Scuba-Cave.)</div></div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(remaining perfectly still)</div> Well, hello there, Alex.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(startled, she quickly stands up)</div> Who said that?</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">I did <div class="inline-direction">("unfreezes," moves a step or two towards ALEX)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(scared, moves back a step or two)</div> What do you want? And how did you know my name??</div>
	<div class="clearfix"></div>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line"><div class="inline-direction">(quickly thinks of an answer)</div> Uh ... Turtle told us. Yeah, that's right, Turtle told me.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You're friends with Turtle??</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Of course! We've been buddies for, uh ... well ... a long time. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You're not very convincing.  </div>
	<div class="clearfix"></div>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">You know, Alex ... <div class="inline-direction">(as she steps closer)</div> We'd really like to be your friend.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(ALEX steps back)</div> I don't know. I'm not so sure that I trust you. </div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(very assuring)</div> Oh, you can trust us.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(uneasy, gets an idea)</div> You, know ...  I think I should go find Turtle. </div>
	<div class="clearfix"></div>
</div> 

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Oh no, you shouldn't do that ... I mean ... <div class="inline-direction">(stalling)</div></div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">... especially at a time like this.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">A time like what?</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(making it up as she goes)</div> Well ... um ... it's ... it's Shell Inspection Season. Didn't you know that? </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Shell Inspection Season?? </div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Why, yes. Every year at this time all the turtles in the whole world are frantically getting ready to go to a certain place far, far away to have their shells inspected. Didn't you watch Finding Nemo?  </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah.</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Well, where did you think all those turtles were going?? Now listen, Alex. We're wasting precious time when we could be getting to know each other. <div class="inline-direction">(notices ALEX'S Bible)</div> What's that book you have?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">This? <div class="inline-direction">(holds it up)</div> It's the Bible.</div>
	<div class="clearfix"></div>
</div>  

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Scuba-Man gave it to you, didn't he?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(surprised)</div> Yeah, how'd you know?</div>
	<div class="clearfix"></div>
</div>
	
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Oh, he gives those out to everyone. Says it's a guidebook or something. I don't know what the big deal is. I surely wouldn't want one if I were you.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(curious)</div> Really, why not?</div>
	<div class="clearfix"></div>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Simple. You just don't need it, girl! You've got common sense don't you??</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Well, yeah ... I guess.</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">And if you don't know something, all you have to do is find someone who does ... like me! <div class="inline-direction">(OCTOPUS puts arm around ALEX'S shoulders)</div> I can tell you anything you need to know. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You can?</div>
	<div class="clearfix"></div>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Absolutely. Here ... let me get rid of this for you <div class="inline-direction">(as she takes the Bible from ALEX'S hand)</div>. You know what, Alex ... I think we're going to be really, really good friends.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne" class="direction-block">
	<p>(Suddenly, the Scuba-Sub [sfx] is heard in the distance. The SUPERHEROES are returning from their "wild goose chase.")</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What's that sound??</div>
	<div class="clearfix"></div>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line"><div class="inline-direction">(quickly becomes alarmed)</div> What?! It couldn't be! </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What's wrong? </div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Uh ... you know ... I ... I ... just forgot. I have a ... a manicure this afternoon, and I'm late. Sorry, Alex, we'll have to do it another time.   <div class="inline-direction">(OCTOPUS drops ALEX'S Bible and runs off stage.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">But, wait! When will you come back?? <div class="inline-direction">(pause)</div> That was strange.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne,alex" class="direction-block">
	<p>(Suddenly the SUPERHEROES, in the Scuba-Sub, burst into the Auditorium through the rear doors and proceed down the side aisle toward the stage. ALEX sees them coming and is fascinated by their vehicle.)</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(walks over to see the Scuba-Sub)</div> Wow! Now THAT is awesome!  </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne" class="role-script-block">
	<div class="role">Scuba-Man and Snorkel-Boy</div>
	<div class="line"><div class="inline-direction">(together)</div> ALEX???!! </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Are you OK?? Are you hurt?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(confused)</div> Of course, I'm OK. Why wouldn't I be??</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">You've had us quite worried, young lady.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">How'd you get back from Porpoise Cove?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Porpoise Cove? What are you talking about?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">You mean ... you weren't at Porpoise Cove?</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Wild underwater goose chase, Scuba-Man! I just knew there was something "fishy" going on!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(notices a Bible on the floor)</div> What's this? A Bible! Is this yours, Alex?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah ... </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Why is it on the ground?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(as he picks it up and hands it to ALEX)</div> Alex, I don't think you realize how important this book is. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">It's actually the very words of God. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">That's right. And it contains God's plan of salvation. Something you can't live without. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">But I've got common sense and ... friends to help me when I need it.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Great tidal wave of confusion, Scuba-Man! Sounds just like the Enemy! If there isn't a dangerous deception going on here, my name isn't Snorkel-Boy!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Precisely my thought, Boy-Wonder! Oops ... sorry. <div class="inline-direction">(short pause)</div> Well, anyway, we need to find out where the deception is coming from! <div class="inline-direction">(turns to ALEX)</div> Alex, I'm going to insist that you stay inside the Scuba-Cave until we make sense of this. Snorkel-Boy will escort you. I'm going to run ahead and get started on the case.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="direction-block">
	<p>(SCUBA-MAN exits into the Scuba-Cave.)</p>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">So, how long do I have to stay in there?</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Only until we can detect and diffuse this dangerous deception. Really, Alex, it's for your own safety. Try to understand. C'mon, let's go.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(realizing resistance is futile, she thinks up an escape plan)</div> You know, you're right. This is no place for a kid to be wandering around by herself.  <div class="inline-direction">(pretends to notice shoelace is untied, bends down)</div> Oh ... I ...  just noticed my shoelace is untied. You go on ahead, I'll catch up with you in a second.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well ... okay. But hurry!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne,alex" class="direction-block">
	<p>(As soon as SNORKEL-BOY disappears into the Scuba-Cave, ALEX tip-toes, then runs away. Soon SNORKEL-BOY emerges from the cave looking for ALEX.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Alex? <div class="inline-direction">(pause)</div> Alex? <div class="inline-direction">(pause as he thinks and talks to himself)</div> Where'd she go? Great kettle of fish! She must have run away! <div class="inline-direction">(he ducks into the Scuba-Cave and calls SCUBA-MAN)</div> Scuba-Man!! <div class="inline-direction">(Then SCUBA-MAN emerges from the Scuba-Cave.)</div> </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Alex is gone! I can't believe I fell for the old "my shoelace is untied" trick! She was wearing flip flops!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">We can't worry about that now, Boy-Wonder!  ... Sorry ...  We've got to find Alex before someone or something else does! You go that way <div class="inline-direction">(points to side aisle)</div>, and I'll go this way <div class="inline-direction">(points to other side aisle)</div>. She couldn't have gotten very far!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne" class="direction-block">
	<p>(SCUBA-MAN and SNORKEL BOY run out the back of the Auditorium.)</p>
</div>


<div data-role="0" class="direction-block">
	<p>(Day 2 Recorded Closing)</p>
</div>	