
<div data-role="0" class="direction-block">
	<h1>Day 3</h1>
	<p>Characters: Alex, Great White, Octopus, Scuba-Man, Snorkel-Boy, Turtle.</p>
	<p>(Day 3 Recorded Opening)</p>
	<p>(Scene begins with SCUBA-MAN returning to the Scuba-Cave. He quickly emerges from the Scuba-Cave with his utility belt, which he's holding loose in one hand.)</p>
</div>

<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">("radio voice" effect; speaking from outside the auditorium)</div> Did you find Alex, Scuba-Man?  </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">No, unfortunately she's missing.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">("radio voice" effect; still emotional)</div> We've just got to find her! It's all my fault!  </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Now listen ... It's going to be a little harder to find Alex than we thought. I'm gonna check the other side of the reef. Meet me as soon as you can at Coral Point. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne,bruce,white,octopus,bruce" class="direction-block">
	<p>(As soon as SNORKEL-BOY and SCUBA-MAN leaves the auditorium, GREAT WHITE and OCTOPUS enter stage from the other side. OCTOPUS is carrying a coil of rope and a spray gun loaded with Sleep Walking Spray. They're trying to sneak up on SCUBA-MAN and SNORKEL-BOY, but quickly find out they're not at home.)</p>
</div>

<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line"><div class="inline-direction">(calls into the Scuba-Cave)</div> Yoo-Hoo! Anybody home?</div>
	<div class="clearfix"></div>
</div>
<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(frustrated)</div> Well, now what are we going to do?? We've got the spray gun all gassed up and no superheroes to use it on!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">We'll just have to think of a way to get them back here. That's all. Hey, maybe they have one of those secret red phones we could call them on. 
</div>
	<div class="clearfix"></div>
</div>
<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Wait! Somebody's coming!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line"><div class="inline-direction">(pleased)</div> Maybe this will be our opportunity!</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus,white,alex" class="direction-block">
	<p>(OCTOPUS quickly ducks into the Scuba-Cave, while GREAT WHITE waits to see who is approaching. Then, suddenly, ALEX bursts in from the back of the auditorium and runs down the side aisle toward the stage.)</p>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(peeking out from inside the cave)</div> Hey, it's Alex! Maybe we can use her as bait! See what you can do.</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">No problem. Just leave it to me.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(looking, walking around, distracted, doesn't notice GREAT WHITE)</div> There's gotta be a place to hide!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">How about over here?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(startled)</div> Ahhh!!!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Sorry ... didn't mean to scare you. You're Alex, aren't you?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Are you in some kind of trouble? </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(looking out for the SUPERHEROES)</div> It's a long story and I don't have time to get into it.</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line"><div class="inline-direction">(concocting a lie)</div> Maybe I can help. Uh ... you know ... I'm the Sheriff of Ocean Land.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(confused, skeptical)</div> You are?? You don't look much like a Sheriff.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(not sure about this "sheriff business")</div> So ... what does a sheriff do down here? I mean you've got Scuba-Man and Snorkel-Boy ... what more do you need?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">So, they've tricked you too, huh?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What do you mean, tricked me too?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Well ...  believe it or not, our superheroes really aren't heroes at all. Actually, they're quite the opposite! They're fakes!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What???</div>
	<div class="clearfix"></div>
</div>
<div data-role="octopus" class="direction-block">
	<p>(OCTOPUS steps out of the Scuba-Cave and inserts herself into the conversation.)</p>
</div>
<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">That's right, Alex. Fakes!! They're nothing more than a couple wreckless vigilantes. </div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">In fact, they're wanted for violating Article III, Section 2 of the Ocean Land Code: Impersonating Superheroes without a License. </div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Which is VERY SERIOUS.</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">And that's why I'm here. To arrest 'em! </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Fakes?? I ... just ... can't believe it! I mean ... what about their costumes ... and ... the Scuba-Cave ... and ... and ... </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">So, how are you going to arrest them? They're not here.</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Well, funny you should ask. I was hoping you could help.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Me? Why me? I ... I ... I don't want to get involved.</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">We've got to get them off the streets right away!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(reluctant)</div> Well ... all right, I guess I'll help.</div>
	<div class="clearfix"></div>
</div> 

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Now, that's better. Trust me. You won't regret this. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What do you want me to do?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Well ... perhaps you might know how to lure them back here to the Scuba-Cave.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">How would I know that?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Aren't they out looking for you??</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah, but I don't know—<div class="inline-direction">(suddenly gets an idea)</div>—hey, wait a minute. <div class="inline-direction">(walks over to the Emergency Button)</div> Over here. See this? It's their Emergency Button. I'm sure if I push this they'll come back. </div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">Really? Well then, what are we waiting for?! We'll hide, and you push the button. <div class="inline-direction">(GREAT WHITE and OCTOPUS quickly take cover.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Okay, Alex, we're in position. Go ahead, push it! <div class="inline-direction">(ALEX pushes the Emergency Button [sfx], then the SUPERHEROES enter the rear of the auditorium and run toward the stage.)</div></div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line"><div class="inline-direction">(excited)</div> Here they come!!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce,wayne,alex" class="direction-block">
	<p>(The SUPERHEROES see ALEX and call out to her.)</p>
</div>
<div class="role-script-block">
	<div class="role">SCUBA-MAN and SNORKEL-BOY</div>
	<div class="line"><div class="inline-direction">(while running)</div> Alex! Alex!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Are you okay??</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">We were really worried about you! </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Why'd you push the Emergency Button? Is there a problem? <div class="inline-direction">(Then GREAT WHITE and OCTOPUS come out of hiding.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Yes, Super Duds! The problem is you! You're both under arrest!!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">SCUBA-MAN and SNORKEL-BOY</div>
	<div class="line">WHAAAAT??</div>
	<div class="clearfix"></div>
</div>
<div data-role="octopus,bruce,wayne" class="direction-block">
	<p>(OCTOPUS immediately sprays the SUPERHEROES with Sleep Walking Spray, which causes them to instantaneously fall asleep and snore while standing.)</p>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line"><div class="inline-direction">(to ALEX)</div> Sleep Walking Spray. We were afraid they wouldn't come peacefully. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(confused)</div> Now what are you going to do?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">That's easy ... we're gonna tie'em up and take'em where we always take'em. <div class="inline-direction">(OCTOPUS takes the coil of rope and begins to tie-up the SUPERHEROES.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Where's that?</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">The Abyss, of course!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(shocked)</div> The Abyss!</div>
	<div class="clearfix"></div>
</div>

<div data-role="octopus" class="role-script-block">
	<div class="role">Octopus</div>
	<div class="line">It's where they belong!</div>
	<div class="clearfix"></div>
</div>
<div data-role="white" class="role-script-block">
	<div class="role">Great White</div>
	<div class="line">Oh ... and Alex. Don't go anywhere ... 'cuz we'll be back with your reward! <div class="inline-direction">(sinister laugh)</div> Ha! Ha! Ha! <div class="inline-direction">(As GREAT WHITE and OCTOPUS walk the sleeping SUPERHEROES away, ALEX doesn't know what to think.)</div> </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(to herself)</div> Something doesn't seem right about this. I'm so confused.</div>
	<div class="clearfix"></div>
</div>

<div data-role="turtle,alex" class="direction-block">
	<p>(then she sits down in front of the cave with her head down)</p>
	<p>(TURTLE enters the stage and sneeks up from behind her.)</p>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(notices mood, kneels down beside ALEX)</div> Hey, why the long face? </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">I don't know ... I'm just so confused. I don't know what's right and wrong or ... who's good and who's bad. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Why not?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What do you mean, "Why not?"</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">We gave you the Bible, remember?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">But, let me guess ... you didn't read it. Thought you'd just do it your own way. And so now you're all mixed up and don't know who or what to trust.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Boy, you can say that again!</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Listen, Alex. There's no one more worthy of your trust than God. He's perfect! And if you can trust God, you can trust His Word in the Bible. And actually, you MUST in order to go to Heaven.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What do you mean?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">God is perfectly pure and holy, but humans aren't. Everybody has done bad things, which is called sin. That's a problem because God says that the penalty from sin is death and separation from Him forever.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Uh-oh.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">"Uh-oh" is right ... but, because of God's amazing love and mercy, He provided a solution to the problem. Do you know what that was?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Uh ... you mean Jesus?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Yup! The Son of God came from Heaven to Earth and lived a pure life without sin. He died on the cross, taking away our sins.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">So now everyone gets to go to heaven?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Ummm...no. To be saved from the punishment we deserve and have a relationship with God now and forever in Heaven, you have to accept the gift of grace. You have to trust and believe in who Jesus is and what He did for you. You can always trust God and the Bible.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Oh, by the way, how was your trip?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(stops, turns around)</div> What trip?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">You know ... to get your shell inspected??</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Shell inspected? What are you talking about? You're joking, right? Oh, Alex ... you're so silly! <div class="inline-direction">(walks offstage)</div> I'll see ya later!! Shell inspected <div class="inline-direction">(as she laughs)</div> ... that's a new one.</div>
	<div class="clearfix"></div>
</div>

<div data-role="turtle" class="direction-block">
	<p>(TURTLE exits.)</p>
</div>

<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(to herself)</div> But ... Octopus said  ... <div class="inline-direction">(realizes she's been deceived)</div> ...  she lied to me!! <div class="inline-direction">(pause)</div> And that means the Sherriff, or whoever he is, also lied!  <div class="inline-direction">(jumps to her feet and runs offstage after GREAT WHITE, OCTOPUS, and the SUPERHEROES)</div> And they're going to The Abyss!!! Scuba-Man! Snorkel-Boy! Stop!!!</div>
	<div class="clearfix"></div>
</div>

<div data-role="0" class="direction-block">
	<p>(Day 3 Recorded Closing)</p>
</div>