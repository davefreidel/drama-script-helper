

<div data-role="0" class="direction-block">
	<h1>Day 5</h1>
	<p>Characters: Alex, Scuba-Man (DAD), Snorkel-Boy, Turtle (MOM), Octopus, Great White.</p>
	
	<p>SCENE ONE</p>
	
	<p>(Day 5 Recorded Opening)</p>
</div>
<!--

<div data-role="bruce,wayne" class="direction-block">
	<p>(Then the SUPERHEROES emerge from the Scuba-Cave.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well, they're not getting out the Evil Prisoner Detention Cell! No-siree, Bob! We've employed the Scuba-Cam Maximum Security Surveillance System with HD Micro-sensor Monitoring. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line"><div class="inline-direction">(relieved)</div> Good ... that makes me feel better!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(excited as he holds up his smartphone)</div> Yeah ... and the new smartphone app allows us to track their every blink and brainwave no matter where we are. Don't you just love technology?! <div class="inline-direction">(as he's captivated by his smartphone)</div> <div class="inline-direction">(SCUBA-MAN clears his throat to get SNORKEL-BOY's attention.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Oh ... sorry. <div class="inline-direction">(as he puts his phone away)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">Here's another Bible, Alex. <div class="inline-direction">(as he hands her a Bible)</div> That was a good thing you did ... giving your Bible to Great White.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Thanks.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">So what's going on? I'm sensing a change in the current.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Alex is leaving.</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Leaving? Why so soon?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Well ... I kinda miss my family ... and I also have homework to do. </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">That's very responsible of you, Alex.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(then she realizes something)</div> Yeah, but there's just one problem ... I don't know how to get home!</div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Well, that's easy. There's only one way ... through "the door."</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What door?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">That door. <div class="inline-direction">(as he points to "the door")</div></div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce" class="direction-block">
	<p>(As SCUBA-MAN points to "the door" [preferably the center aisle exit doors], they suddenly open and a bright light shines through the doorway while dramatic music [sfx] is heard.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">It's the only way.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Wow ... </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line">And let that door be a reminder, Alex ... that just as there was only one way to be saved from the Flood — and that was through the door into the Ark; likewise, there's only one way to be saved and get to heaven — and that's through "the Door," Jesus Christ. </div>
	<div class="clearfix"></div>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line">Just like Jesus said, in John 10:9, "I AM THE DOOR; IF ANYONE ENTERS BY ME, HE SHALL BE SAVED ... ." </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">That's a message the whole world needs to hear.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Thanks, I'll remember that. <div class="inline-direction">(short pause)</div> Well ... I guess it's time. You know, I'm really gonna miss you guys. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">We'll miss you, too, Alex.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(as she reaches out to shake hands, she's overcome with emotion and gives them all hugs)</div> Good-bye Scuba-Man. Good-bye Snorkel-Boy. Good-bye Turtle.</div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce,wayne,turtle" class="direction-block">
	<p>(SUPERHEROES and TURTLE say good-bye; SNORKEL-BOY wipes tears from his eyes.)</p>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Turtle</div>
	<div class="line">Don't forget us.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">I promise I won't. <div class="inline-direction">(pause)</div> Good-bye!</div>
	<div class="clearfix"></div>
</div>

<div data-role="alex" class="direction-block">
	<p>(Then ALEX walks toward the door and exits.)</p>
</div>
<div data-role="wayne" class="role-script-block">
	<div class="role">Snorkel Boy</div>
	<div class="line"><div class="inline-direction">(encouraging)</div> To Infinity and Beyond!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Scuba Man</div>
	<div class="line"><div class="inline-direction">(softly to SNORKEL-BOY)</div> Why do you always say that?</div>
	<div class="clearfix"></div>
</div>-->

<div data-role="0" class="direction-block">
	<!--<p>SCENE TWO</p>-->
	<p>(<!--QUICK SCENE CHANGE:  As soon as ALEX exits through the door, TURTLE and the SUPERHEROES quickly exit the stage. Then the lights go down and dream music [sfx] and -->Ethereal screen effects begin as ALEX, with her aquarium catalog, enters in a theatrical fashion as if dreaming. She "floats" and circles until she comes to rest on the floor in the same place and position that she was in when she fell asleep on Day 1. Soon, MOM and DAD come in to check on her.)</p>
</div>

<div class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">And then I asked them ...  if there really was a worldwide flood, what would the evidence be? Wouldn't it be billions of dead things, buried in rock layers, laid down by water, all over the earth? And, guess what ...  that's exactly what we find ... billions of dead things, buried in rock layers, laid down by water, all over the earth!</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">But they weren't convinced?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Nope ... and that's my point. Because people don't fear God and take Him seriously, they don't take the Bible seriously, either. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">And because they don't take the Bible seriously, you get in trouble at work for talking about Noah's Flood.</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Right. Something they think is just a children's story.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">A worldwide catastrophe is anything but a children's story. <div class="inline-direction">(then she notices that ALEX is asleep)</div> Oh, look ...  she's asleep.</div>
	<div class="clearfix"></div>
</div>

<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">I knew that was going to happen. Should we wake her up?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line"><div class="inline-direction">(with a smile)</div> Unless you want to carry her upstairs. <div class="inline-direction">(then taps ALEX on the shoulder)</div> Alex? Aaaaleeeex ... c'mon, sweetheart. You need to get to bed.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(waking up)</div> Where am I? . . . Mom? Dad? What are you doing here?</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Uh ... we live here, sweetie. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">And so do you.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What? <div class="inline-direction">(looks around, gets excited)</div> I'm home? ... I'm home! It worked, I went through the door and made it home! <div class="inline-direction">(now fully awake, excited)</div> Oh, Mom and Dad, I was in this amazing place called Ocean Land. It was at the bottom of the ocean.  </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Because of the aquarium catalog ... </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">What?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">It's because of the aquarium catalog you've been devouring tonight. That's why you were dreaming about the ocean.</div>
	<div class="clearfix"></div>
</div>

<div class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">I told you so.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Oh, no ... you don't understand ... this was a real live place ... and both of you were there! <div class="inline-direction">(short pause as she starts to doubt)</div> At least it ... seemed like a real place. You mean it was ... just a dream?</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Disappointed?</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah ... a little.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Sorry, sweetie.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">That's okay. Hmm ... you know, now that I think about it, it was kind of funny. <div class="inline-direction">(smiles)</div> Mom, you were a turtle. </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">A turtle? I woulda thought maybe an angelfish!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line"><div class="inline-direction">(teasing, with a smile)</div> Or a crab.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Hey! </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line"><div class="inline-direction">(laughs)</div> Just kidding.</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">What was dad? </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Probably a starfish. </div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">No, actually, you were a superhero ... </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Oh, brother ... we're never going to hear the end of this!</div>
	<div class="clearfix"></div>
</div>
<div class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Look! Up in the air! It's a bird! It's a ... <div class="inline-direction">(ALEX interrupts)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">No, Dad, not Superman. You were "Scuba-Man" ... and Mr. Wayne was "Snorkel-Boy."</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">Snorkel-Boy?? <div class="inline-direction">(laughs)</div> Wayne will love that!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">And you know what else? This was really good. I sort of re-discovered the Bible while I was there ... </div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line"><div class="inline-direction">(pleased)</div> Really! That's great sweetheart.</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">Yeah ... and so from now on, I'm going to take it a lot more seriously. <div class="inline-direction">(then remembers more of her dream)</div> Oh, and when I wanted to get home, they said there was only one way and it was through this one special door ... just like there's only one way to be saved and get to heaven and that's through Jesus. Snorkel Boy told me that Jesus said, in John 10:9, "I AM THE DOOR; IF ANYONE ENTERS BY ME, HE SHALL BE SAVED ... ."</div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Wow! That really was some dream!</div>
	<div class="clearfix"></div>
</div>
<div data-role="bruce" class="role-script-block">
	<div class="role">Dad</div>
	<div class="line">I'll say!</div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line">I know ... it was amazing. <div class="inline-direction">(then she yawns and stretches)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Well, c'mon ... you'd better get ready for bed. <div class="inline-direction">(ALEX gets up and then they all start to exit together.)</div></div>
	<div class="clearfix"></div>
</div>
<div data-role="alex" class="role-script-block">
	<div class="role">Alex</div>
	<div class="line"><div class="inline-direction">(thinking)</div> But ... are you sure it was just a dream?? </div>
	<div class="clearfix"></div>
</div>
<div data-role="turtle" class="role-script-block">
	<div class="role">Mom</div>
	<div class="line">Yes, Alex. We're sure.</div>
	<div class="clearfix"></div>
</div>

<div data-role="turtle,bruce,alex,white,octopus" class="direction-block">
	<p>(MOM, DAD, and ALEX exit the stage together. Then, as the lights start to go down, the voices of GREAT WHITE and OCTOPUS are heard from inside the Scuba-Cave.)</p>
</div>

<div data-role="0" class="direction-block">
	<p>(Theme Music)</p>
</div>


